import os
import numpy as np
import librosa
import scipy
import math
import argparse

SAMPLES_FOLDER = "Samples"
BASE_SCRIPT_START = """
<?xml version="1.0" encoding="UTF-8" ?>
<DecentSampler>
    <ui bgImage="background.png" width="812" height="375" layoutMode="relative" bgMode="top_left">
        <tab name="main">
          <labeled-knob x="9" y="80" label="tone" type="float" minValue="220" maxValue="22000" value="22000" textColor="FFFFFFFF"
      	    textSize="16" width="120" height="120" trackForegroundColor="E63D99F4" trackBackgroundColor="80808080">
                <binding type="effect" level="instrument" position="0" parameter="FX_FILTER_FREQUENCY"/>
		  </labeled-knob>
		
	      <labeled-knob x="142" y="80" label="reverb" type="float" minValue="0.0" maxValue="1" value="0.0" textColor="FFFFFFFF"
		    textSize="16" width="120" height="120" trackForegroundColor="E63D99F4" trackBackgroundColor="80808080">
                <binding type="effect" level="instrument" position="1" parameter="FX_REVERB_WET_LEVEL"/>
		  </labeled-knob>
      
          <labeled-knob x="278" y="80" label="notes volume" type="float" minValue="0" maxValue="1" value="1" textColor="FFFFFFFF"
          	textSize="16" width="120" height="120" trackForegroundColor="E63D99F4" trackBackgroundColor="80808080">
                <binding type="amp" level="group" position="0" parameter="AMP_VOLUME" translation="linear" translationOutputMin="0" translationOutputMax="1.0"  />
                <binding type="amp" level="group" position="1" parameter="AMP_VOLUME" translation="linear" translationOutputMin="0" translationOutputMax="1.0"  />
          </labeled-knob>
      
      
         <labeled-knob x="413" y="80" label="rt volume" type="float" minValue="0" maxValue="1" value="0.33" textColor="FFFFFFFF"
          	textSize="16" width="120" height="120" trackForegroundColor="E63D99F4" trackBackgroundColor="80808080">
                <binding type="amp" level="group" position="2" parameter="AMP_VOLUME" translation="linear" translationOutputMin="0" translationOutputMax="1.0"  />
          </labeled-knob>
      
          <labeled-knob x="549" y="80" label="attack" type="float" minValue="0.0" maxValue="2.0" value="0.005" textColor="FFFFFFFF"
		    textSize="16" width="120" height="120" trackForegroundColor="E63D99F4" trackBackgroundColor="80808080">
                <binding type="amp" level="instrument" position="0" parameter="ENV_ATTACK" />
		  </labeled-knob>
      
          <labeled-knob x="681" y="80" label="release" type="float" minValue="0.0" maxValue="5.0" value="0.15" textColor="FFFFFFFF"
		    textSize="16" width="120" height="120" trackForegroundColor="E63D99F4" trackBackgroundColor="80808080">
                <binding type="amp" level="instrument" position="0" parameter="ENV_RELEASE" />
		  </labeled-knob>
	
        </tab>
    </ui>
    <groups seqMode="round_robin">
        <group>

"""
BASE_SCRIPT_END = """
        </group>
    </groups>
    <effects>
        <effect type="lowpass_4pl" frequency="22000.0"/>
        <effect type="reverb" wetLevel="0" roomSize="0.85" damping="0.2"/>
    </effects>
</DecentSampler>
"""

def create_sample_line_noround(note_number, lo_note, hi_note, file_path):
    return f'            <sample rootNote="{note_number}" loNote="{lo_note}" hiNote="{hi_note}" path="{file_path}" />'

def create_sample_line(note_number, lo_note, hi_note, file_path, seq_pos):
    return f'            <sample rootNote="{note_number}" loNote="{lo_note}" hiNote="{hi_note}" seqPosition="{seq_pos}" path="{file_path}" />'

def main():   
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--name', default='preset', type=str, help='an integer for the accumulator')
    args = parser.parse_args()
    OUTPUT_FILE = args.name + ".dspreset"

    audio_samples_names = os.listdir(SAMPLES_FOLDER)
    audio_samples_names.sort()
    
    samples_lines = ""
    max_iteration = len(audio_samples_names)

    samples_info = []
    for i, sample_name in enumerate(audio_samples_names):
        print(f'{i+1}/{max_iteration} (Select hiNote and loNote) {sample_name}')
        path = os.path.join(SAMPLES_FOLDER, sample_name)
        note_name = sample_name.split('.wav')[0].split(' ')[3]
        print(f'\t{note_name}')
        midi_number = int(math.ceil(librosa.hz_to_midi(librosa.note_to_hz(note_name))))
        print(f'\t{midi_number}')

        samples_info.append([midi_number, path])

    samples_info.sort(key=lambda x: x[0])
    s_info_grouped_by_note=[[c for c in samples_info if c[0] == a] for a in set([x[0] for x in samples_info])]

    max_iteration=len(s_info_grouped_by_note)
    for i, s in enumerate(s_info_grouped_by_note):
        print(f'{i+1}/{max_iteration} (Select hiNote and loNote)')
        if i==0:
            lo_note_delta = 4
        else:
            lo_note_delta = int(math.ceil((s[0][0]-s_info_grouped_by_note[i-1][0][0])/2.0)) - 1
        if i==max_iteration-1:
            hi_note_delta = 4
        else:
            hi_note_delta = int((s_info_grouped_by_note[i+1][0][0]-s[0][0])/2.0)
        lo_note, hi_note = s[0][0] - lo_note_delta, s[0][0] + hi_note_delta
        print(
            f'\tloNote: {librosa.midi_to_note(lo_note)}({lo_note}),',
            f'rootNote: {librosa.midi_to_note(s[0][0])}({s[0][0]}),',
            f'hiNote: {librosa.midi_to_note(hi_note)}({hi_note})',
        )
        round_robin_seq_pos = 1
        for sx in s:
            print(f'\t\t{sx[1]}')
            if sx[1].split('.wav')[0].split(' ')[2] == 'RT':
                sample_line = create_sample_line_noround(sx[0], lo_note, hi_note, sx[1])
            else:
                sample_line = create_sample_line(sx[0], lo_note, hi_note, sx[1], round_robin_seq_pos)
                round_robin_seq_pos += 1
            samples_lines += sample_line + '\n'

    
    print('ceate .dspreset text')            
    lib_text = BASE_SCRIPT_START + samples_lines + BASE_SCRIPT_END

    print('ceate .dspreset file')
    with open(OUTPUT_FILE, 'w') as f:
        f.writelines(lib_text)

if __name__ == "__main__":
    main()

