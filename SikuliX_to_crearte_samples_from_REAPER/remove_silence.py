import argparse
import librosa
import soundfile
import os

parser = argparse.ArgumentParser(description='Remove Silence.')
parser.add_argument('--input', default='./Samples', type=str, help='input folder')
parser.add_argument('--output', default='./SamplesCropped', type=str, help='output folder')
args = parser.parse_args()

def main():
    audio_samples_names = os.listdir(args.input)
    max_iteration = len(audio_samples_names)

    for i, sample_name in enumerate(audio_samples_names):
        print(f'{i+1}/{max_iteration} (Read file and remove silence)')
        path = os.path.join(args.input, sample_name)

        print('\t', path)
        sr = librosa.get_samplerate(path)
        audio, sample_rate = librosa.load(path, sr=sr)
        
        start_index = 0
        for c in range(len(audio)):
            if audio[c] != 0.0:
                break
            start_index = c
        end_index = len(audio)
        for c in range(len(audio), 1):
            if audio[c] != 0.0:
                break
            end_index = c
        
        print('\t', 'start crop:', start_index, 'end crop:', end_index, 'samples count:', len(audio))
        
        out_path = os.path.join(args.output, sample_name)
        print('\t', "saving to", out_path)
        soundfile.write(out_path, audio[start_index:end_index], sr)



if __name__ == "__main__":
    main()

