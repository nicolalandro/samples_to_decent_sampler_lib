start_x = 766
start_y = 1050
step = 13

skip_bottom = -1
skip_top = 2

note_dutration = 1

for white_key_number in range(50):
    # adjust the start point to click correct
    if white_key_number % 5 == 0:
        start_x = start_x - 3
    # record 
    if white_key_number < skip_top and white_key_number > skip_bottom:
        # start recording
        click(Location(18, 783))
        click(Location(78, 779))
        
        # open plugins
        click("1627373606919.png")
        wait(1)

        # play note
        offset_key = step * white_key_number
        mouseMove(Location(start_x + offset_key, start_y))
        mouseDown(Button.LEFT)
        wait(note_dutration)
        mouseUp()

        # close plugins
        click("1627373644228.png")
        
        # stop recording
        click(Location(78, 779))

        # save
        click(Location(13, 70))
        click(Location(106, 355))
        click(Location(913, 387))
        type(str(white_key_number))
        click(Location(1215, 848))
        click(Location(999, 845))
        wait(note_dutration + 2)
        click(Location(1040, 699))
        
        
        # remove
        click(Location(606, 215))
        type(Key.DELETE)
        