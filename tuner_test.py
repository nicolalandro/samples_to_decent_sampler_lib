import os
import numpy as np
import librosa
import scipy
import math
import argparse
import timeit

def main():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--path', default='preset', type=str, help='an integer for the accumulator')
    args = parser.parse_args()
    
    audio, sample_rate = librosa.load(args.path) # default semple rate 22500 that reduce original but it is more efficient for computation
    print(args.path)

    print('pYIN')
    start = timeit.default_timer()        
    audio_to_note_pYIN(audio, sample_rate)
    stop = timeit.default_timer()
    print('Time: ', stop - start)
  
    print('fft')
    start = timeit.default_timer()  
    freq_from_autocorr(audio, sample_rate)
    stop = timeit.default_timer()
    print('Time: ', stop - start)
 
def audio_to_note_pYIN(audio, sample_rate):
    fundamental_frequencies, _, _ = librosa.pyin(audio, fmin=librosa.note_to_hz('C2'), fmax=librosa.note_to_hz('C7'))
    
    filtered_fundamental_frequencies = list(filter(lambda x: not np.isnan(x), fundamental_frequencies))
    predicted_frequency = scipy.stats.mode(filtered_fundamental_frequencies).mode[0]
    # frequency_mode = scipy.stats.mode(filtered_fundamental_frequencies).mode[0]
    # frequency_median = np.median(filtered_fundamental_frequencies)
    # frequency_mean = np.mean(filtered_fundamental_frequencies)
    # print("\tmode:", frequency_mode, "median:", frequency_median, "mean:", frequency_mean)
    
    # predicted_frequency = np.mean([frequency_mode, frequency_median])
    raw_midi_number = librosa.hz_to_midi(predicted_frequency)
    note_name = librosa.midi_to_note(raw_midi_number)
    midi_number = int(math.ceil(librosa.hz_to_midi(librosa.note_to_hz(note_name))))
    print('\tfrequency:', predicted_frequency, 
          'midi number:', f'{midi_number}({raw_midi_number})', 
          'note:', note_name)
    return midi_number

def parabolic(f, x):
    xv = 1/2. * (f[x-1] - f[x+1]) / (f[x-1] - 2 * f[x] + f[x+1]) + x
    yv = f[x] - 1/4. * (f[x-1] - f[x+1]) * (xv - x)
    return (xv, yv)

def freq_from_autocorr(sig, fs):
    corr = scipy.signal.fftconvolve(sig, sig[::-1], mode='full')
    corr = corr[len(corr)//2:]
    d = np.diff(corr)
    start = np.where(d > 0)[0][0]
    peak = np.argmax(corr[start:]) + start
    px, py = parabolic(corr, peak)

    predicted_frequency = fs / px
    raw_midi_number = librosa.hz_to_midi(predicted_frequency)
    note_name = librosa.midi_to_note(raw_midi_number)
    midi_number = int(math.ceil(librosa.hz_to_midi(librosa.note_to_hz(note_name))))
    print('\tfrequency:', predicted_frequency, 
          'midi number:', f'{midi_number}({raw_midi_number})', 
          'note:', note_name)
    return midi_number

if __name__ == "__main__":
    main()

