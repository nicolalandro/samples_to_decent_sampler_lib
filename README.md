# samples_to_decent_sampler_lib
[Decent Sampler](https://www.decentsamples.com/product/decent-sampler-plugin/) is a free vst2, vst3, AAX plugin that work on Windows, OSX and also Linux. It is a very good alternative to [Kontakt](https://www.native-instruments.com/en/products/komplete/samplers/kontakt-6/) and you can find better quality library of sounds on [PianoBook](https://www.pianobook.co.uk/) (better than fluidsynth, liquidsfz and sfizz native for linux, and competitive with Kontakt).
So I decide to create a python script that create a [Decent Sampler](https://www.decentsamples.com/product/decent-sampler-plugin/) Lib automatically from a folder of samples for all people that want to port to Decent Sampler their library but also to create quickly new lib or to have a starting point file.

## Requirements
* python3 (I use python 3.7)
* scipy: `pip install scipy` (I use scipy 1.5.2)
* librosa: `pip install librosa` (I use librosa 0.8.1)

## How to Use
* generate plugin:
  * create a folder
  * put background.png file (width: 812, height: 375)
  * create a folder Samples with inside all the .wav recorded
  * copy generate_decentsampler.py inside the folder
  * run `python3 generate_decentsampler.py` ([optional] you can add `--name plugin_name` to generate `plugin_name.dspreset` instead of `preset.dspreset`)
  * it create the 'preset.dspreset' file that is the file to load into your decentsampler
  * [optional] if you want you can zip this folder to save disk space
  * [optional] you can share your zip folder on [PianoBook](https://www.pianobook.co.uk/)
* tuner test get the frequency and the note from an audio file:
  * `python3.7 tuner_test.py --path /path/to/file.wav`
* remove silence with real 0.0 value:
  * `python3.7 SikuliX_to_crearte_samples_from_REAPER/remove_silence.py --input Samples --output CroppedSamples`


## Features and work
* generate_decentsampler.py
  * read all file in folder samples
  * use tuner: [pYIN 2014](https://qmro.qmul.ac.uk/xmlui/bitstream/handle/123456789/6040/MAUCHpYINFundamental2014Accepted.pdf) algorithm to estimate fundamental frequency in the times
  * compute the average between mode and median, that is the note frequency
  * transform the note frequency to midi that is the Root value
  * specify for each sample the midi Low, Root and High to complete the range (lower -4, highest +4)
  * generate the .dspreset file
* tuner_test.py
  * use pYIN
  * use fftconvolve
* SikuliX_to_crearte_samples_from_REAPER/remove_silence.py
  * use librosa to load file
  * remove starting and ending 0.0 values
  * use soundfile to save the wav file

## FOR EXPERT
* Sikuli extract automatically samples from REAPER
  * record output of the track
  * exec sikulix script
![](SikuliX_to_crearte_samples_from_REAPER/run_example.gif	)

# References
This is the list of knowledge that I need to develop this project
* [python](https://www.python.org/about/gettingstarted/)
* [librosa](https://librosa.org/doc/latest/index.html)
* [create decent sample lib](https://www.youtube.com/watch?v=UxPRmD_RNCY)
* (Extra) [Peter Eastman's Python SFZ mapping scripts](https://vis.versilstudios.com/sfzconverter.html#u13452-4)
* (FOR EXPERT) [SikuliX Tutorial](https://www.youtube.com/watch?v=VdCOg1bCmGo&list=PL1A2CSdiySGJJNe3WzCezcI7by5SPfJTS)

